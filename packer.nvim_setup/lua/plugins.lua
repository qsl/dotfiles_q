
packer = require 'packer'
packer.init {
	opt_default = true -- default to using opt (and not start) plugins
}

local use = packer.use
packer.reset()

packer.startup(function()

	use {
		'wbthomason/packer.nvim',
		opt = false
	}

	use 'easymotion/vim-easymotion'



end)
