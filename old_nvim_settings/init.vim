" #######
" PLUGINS
" #######

call plug#begin('~/.config/nvim/plugged')

"===== Coc(k) language server sachen .. ==============================================

Plug 'rafi/awesome-vim-colorschemes'

Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
Plug 'Xuyuanp/nerdtree-git-plugin', {'do': ':UpdateRemotePlugins'}                                               

"===== Coc(k) language server sachen .. ==============================================

Plug 'neoclide/coc.nvim', {'branch': 'release'}

let g:coc_global_extensions = [
  \ 'coc-json',
  \ 'coc-java',
  \ 'coc-java-debug',
  \ 'coc-html',
  \ 'coc-pyright',
  \ 'coc-groovy',
  \ 'coc-sh',
  \ 'coc-eslint',
  \ 'coc-r-lsp',
  \ 'coc-markdownlint',
  \ 'coc-css'
  \ ]     


" Initialize plugin system
call plug#end()

" #######
" PLUGINSEND
" #######


set background=dark
" colorscheme gotham256
" colorscheme tender
colorscheme meta5

nmap        <expr>          <F2>        ToggleNumbering()
nmap        <expr>          <F3>        ToggleWrap()
noremap     <F5>            :NERDTreeToggle<cr>  

"===== Toggle Numbering Function ==============================================

function ToggleNumbering()
    if &relativenumber
        set norelativenumber
    else
        if &number
            set nonumber
        else
            set number
            set relativenumber
        end
    end
endfunction


function ToggleWrap()
  if &wrap
    echo "Wrap OFF"
    setlocal nowrap
    set virtualedit=all
    silent! nunmap <buffer> <Up>
    silent! nunmap <buffer> <Down>
    silent! nunmap <buffer> <Home>
    silent! nunmap <buffer> <End>
    silent! iunmap <buffer> <Up>
    silent! iunmap <buffer> <Down>
    silent! iunmap <buffer> <Home>
    silent! iunmap <buffer> <End>
  else
    echo "Wrap ON"
    setlocal wrap linebreak nolist
    set virtualedit=
    setlocal display+=lastline
    noremap  <buffer> <silent> <Up>   gk
    noremap  <buffer> <silent> <Down> gj
    noremap  <buffer> <silent> <Home> g<Home>
    noremap  <buffer> <silent> <End>  g<End>
    inoremap <buffer> <silent> <Up>   <C-o>gk
    inoremap <buffer> <silent> <Down> <C-o>gj
    inoremap <buffer> <silent> <Home> <C-o>g<Home>
    inoremap <buffer> <silent> <End>  <C-o>g<End>
  endif
endfunction

