# Add packer.nvim plugins

## 1. 

in `nvim/lua/plugins.lua` add pluins with 

```
	use 'easymotion/vim-easymotion'
```

or

```
	use {
		'wbthomason/packer.nvim',
		opt = false
	}
```



## 2.

:PackerCompile

## 3. 

:PackerUpdate




####### src:
- https://github.com/wbthomason/packer.nvim#compiling-lazy-loaders
- https://www.youtube.com/watch?v=5rH_JL3qil0&t=30s